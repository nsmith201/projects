#!/bin/sh

awk -F " " '
BEGIN{ n = 0}
{ 
n++;
if ( n == 1 ){
	through_mb = $2/1;
}
if ( n == 2 ){
        through_10mb = $2/10;
}
if ( n == 3 ){
        through_gb = $2/1024;
}
}
END{ print "1",through_mb
	print "2", through_10mb
	print "3", through_gb}' forked_through.txt


