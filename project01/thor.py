#!/usr/bin/env python2.7

import calendar
import os
import socket
import sys
import time
import signal
import getopt
#VARIABLES
PATH = "/"
PORT = 80
PROCESSES = 1;
REQUESTS = 1;
VERB = False
try:
        opts, args = getopt.getopt(sys.argv[1:], 'r:p:hv')

except getopt.GetoptError as e:
        print e
        usage(1)

for o, a in opts:
        if o == '-r':
                REQUESTS = int(a)
        if o == '-p':
                PROCESSES = int(a)
	if o == '-v':
		VERB = True
        elif o in '-h':
                print "usage ./thor.py -p [PROCESSES] -r (default=1) [REQUESTS]"
                sys.exit(1)

DERP = sys.argv[-1]
DERP2 = DERP.split(':')
DERP3 = DERP.split('/')
PATH = DERP3[-1]
if len(DERP2) > 1:
	if '/' in DERP2[-1]:
		DERP4 = DERP2[-1].split('/')
		PORT = int(DERP4[0])
	else:
		PORT=int(DERP2[-1])
URL = DERP2[0]
#PORT = oldURL.split(':')[-1]
#URL = oldURL.split(':')[0]
def sigchld_handler(signum, frame):
	global ChildPid, ChildStatus
	ChildPid, ChildStatus = os.wait()
	print 'Finished with: ', signum

class TCPClient(object):
	def __init__(self,address,port):
		self.port = port
		self.address = socket.gethostbyname(address)
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def run(self):
		self.socket.connect((self.address, self.port))
		if VERB:
			print "CONNECTED!!!\n"
		self.stream = self.socket.makefile('w+')
		if VERB:
			print "Handling\n"
		self.handle()
		if VERB:
			print "Finishing\n"
		self.finish()

	def finish(self):
		self.socket.shutdown(socket.SHUT_RDWR)
		self.socket.close()

	def handle(self):
		self.stream.write('Hello\n')

class HTTPClient(TCPClient):
	def __init__(self, address, port, path):
		TCPClient.__init__(self, address, port)
		self.host = address
		self.path = path
	def handle(self):
		self.stream.write('GET {} HTTP/1.0\r\n'.format(self.path))
		self.stream.write('Host: {}\r\n'.format(self.host))
		self.stream.write('\r\n')
		self.stream.flush()
		data = self.stream.readline()
		
		while data:
			sys.stdout.write(data)
			data = self.stream.readline()


if __name__ == '__main__':
	
	for PROCESS in range(PROCESSES):
		if VERB:
			print "Forking...\n"
		pid = os.fork()
		if pid == 0:
			totlag = 0
			for REQUEST in range(REQUESTS):
				if VERB:
					print "Process: " , pid , " Sending Request "
				client = HTTPClient(URL , PORT, PATH)
				nowtime = time.time()
				client.run()
				endtime= time.time()
				lag=endtime - nowtime
				totlag = totlag+lag
				avglag = totlag/REQUESTS
			#print "\n","Process: ",PROCESS,"average lag",avglag
			print "\n",PROCESS,avglag, "lag"
			print "\n", pid, "Exiting"
			sys.exit(0)._exit()
		else:
			signal.signal(signal.SIGCHLD,sigchld_handler)

