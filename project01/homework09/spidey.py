#!/usr/bin/env python2.7

import os
import socket
import sys
import time
import signal
import getopt

#VARIABLES

FILE = '/'
FORK = False
PORT = 9998
DIR = '.'
try:
	opts, args = getopt.getopt(sys.argv[1:], 'hfvd:p:')

except getopt.GetoptError as e:
	print e
	usage(1)

for o, a in opts:
	if o == '-d':
		DIR = os.path.abspath(a)
	if o == '-f':
		FORK = True
	if o == '-p':
		PORT = int(a)
	elif o in '-h':
		print "usage ./spidey.py -p [PORT] -d (default='.') [DOCROOT]"
		print " -h show help -f forking mode -v logging to debug"
		sys.exit(1)

class TCPServer(object):
	def __init__(self,address,port, handler):
		self.port = port
		self.address = socket.gethostbyname(address)
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.handler = handler
		print 'Listening to {address}:{port}'.format(address=address,port=port) 
	def run(self):
		self.socket.bind((self.address, self.port))
		self.socket.listen(0)
		if FORK:
			while True:
				client, address = self.socket.accept()
				signal.signal(signal.SIGCHLD, signal.SIG_IGN)	
				pid = os.fork()
				if pid == 0:
					handler  = self.handler(client, address)
					handler.handle()
					handler.finish()
					sys.exit(0)
				else:
					client.close()
		else:
			while True:
				client, address = self.socket.accept()
				handler = self.handler(client,address)
				handler.handle()
				handler.finish()
class BaseHandler(object):
	def __init__(self, fd, address):
		self.socket = fd
		self.address = address
		self.stream = self.socket.makefile('w+')
	def handle(self):
		pass

	def finish(self):
		self.stream.flush()
		self.socket.shutdown(socket.SHUT_RDWR)
		self.socket.close()
class HTTPHandler(BaseHandler):
	def handle(self):
		data = self.stream.readline().strip().split()
		FILE = data[1]
		FILE = os.path.basename(FILE)
		data = self.stream.readline().rstrip()
		while data:
			sys.stdout.write(data)
			sys.stdout.write('\n')
			data = self.stream.readline().rstrip()
		self.stream.write('HTTP/1.0 200 OK\r\n')
		self.stream.write('Content-Type: text/html\r\n')
		self.stream.write('\r\n')
#		if FILE != '/':
		PATH = os.path.join(DIR,FILE)
#		PATH = '/afs/nd.edu/user26/nsmith9/Private/CS20189/assignments/homework09/thor.py'
#		else:
#			PATH = DIR		
		if os.path.isfile(PATH) and os.access(PATH, os.X_OK):
			self._handle_script(PATH)
		elif os.path.isfile(PATH) and os.access(PATH, os.R_OK):
			self._handle_file(PATH)
		elif os.path.isdir(PATH) and os.access(PATH, os.R_OK):
			self._handle_dir(PATH)
		else:
			self._handle_error(PATH)

	def _handle_file(self,PATH):
		self.stream.write('<pre>')
		for line in open(PATH):
			self.stream.write(line)	
		self.stream.write('</pre>')

	def _handle_error(self,PATH):
		sys.stdout.write('{path}'.format(path=PATH))
		self.stream.write('<html lang="en">')
		self.stream.write('<head>')
		self.stream.write('<title>404 Error</title>')
		self.stream.write('<link href="https://www3.nd.edu/~pbui/static/css/blugold.css" rel="stylesheet">')
		self.stream.write('<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">')
		self.stream.write('</head>')
		self.stream.write('<body>')
		self.stream.write('<div class="container">')
		self.stream.write('<div class="page-header">')
		self.stream.write('<h2>404 Error</h2>')
		self.stream.write('</div>')
		self.stream.write('<div class="thumbnail">')
		self.stream.write('<img src="http://images5.fanpop.com/image/photos/31300000/pending-black-panthers-den-31390430-500-402.jpg" class="img-responsive">')
		self.stream.write('</div>')
		self.stream.write('</div>')
		self.stream.write('</body>')
		self.stream.write('</html>')


	def _handle_dir(self,PATH):
		directory = os.path.basename(PATH)
		self.stream.write('<!DOCTYPE html>')
		self.stream.write('<html lang="en">')
		self.stream.write('<head>')
		self.stream.write('<title>/</title>')
		self.stream.write('<link href="https://www3.nd.edu/~pbui/static/css/blugold.css" rel="stylesheet">')
		self.stream.write('<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">')
		self.stream.write('</head>')
		self.stream.write('<body>')
		self.stream.write('<div class="container">')
		self.stream.write('<div class="page-header">')
		self.stream.write('<h2>Directory Listing: {dir}</h2>'.format(dir=directory))
		self.stream.write('</div>')
		self.stream.write('<table class="table table-striped">')
		self.stream.write('<thead>')
		self.stream.write('<th>Type</th>')
		self.stream.write('<th>Name</th>')
		self.stream.write('<th>Size</th>')
		self.stream.write('</thead>')
		self.stream.write('<tbody>')
		for files in os.listdir(PATH):
			full_path = os.path.join(PATH,files)
			self.stream.write('<tr>')
			self.stream.write('<td><i class="fa fa-folder-o"></i></td>')
			self.stream.write('<td><a href="/{file}">{file}</a></td>'.format(file = files))
			self.stream.write('<td>-</td>')
			self.stream.write('</tr>')
		self.stream.write('</tbody>')
		self.stream.write('</table>')
		self.stream.write('</div>')
		self.stream.write('</body>')
		self.stream.write('</html>')

	def _handle_script(self,PATH):
		signal.signal(signal.SIGCHLD, signal.SIG_DFL)
		self.stream.write('<pre>')
		for line in os.popen(PATH):
			self.stream.write(line)
		self.stream.write('<pre>')
		signal.signal(signal.SIGCHLD, signal.SIG_IGN)	

if __name__ == '__main__':
	server = TCPServer('0.0.0.0', PORT, HTTPHandler)
	server.run()
