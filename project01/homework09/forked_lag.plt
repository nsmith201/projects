set terminal png truecolor
set xrange [0:11]
set yrange [0:1]
set grid
set style data histograms
set style fill solid border
set boxwidth 0.95
set xtics 1,1,10
set output "forked_lag.png"

plot 'forked_lag_data.txt' using ($1):2 title "Process" with boxes

