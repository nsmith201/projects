set terminal png truecolor
set xrange [0:4]
set yrange [0:1]
set grid
set style data histograms
set style fill solid border
set boxwidth 0.95
set xtics 1,1,3
set output "forked_through.png"

plot 'forked_through_data.txt' using ($1):2 title "Size" with boxes

