#!/usr/bin/env python2.7

import calendar
import os
import socket
import sys
import time
import signal
import getopt
#VARIABLES

PORT = 9999
PROCESSES = 1;
REQUESTS = 1;
try:
        opts, args = getopt.getopt(sys.argv[1:], 'r:p:h')

except getopt.GetoptError as e:
        print e
        usage(1)

for o, a in opts:
        if o == '-r':
                REQUESTS = int(a)
        if o == '-p':
                PROCESSES = int(a)
        elif o in '-h':
                print "usage ./thor.py -p [PROCESSES] -r (default=1) [REQUESTS]"
                sys.exit(1)

URL = sys.argv[-1]
#PORT = oldURL.split(':')[-1]
#URL = oldURL.split(':')[0]
def sigchld_handler(signum, frame):
	global ChildPid, ChildStatus
	ChildPid, ChildStatus = os.wait()
	print 'Finished with: ', signum

class TCPClient(object):
	def __init__(self,address,port):
		self.port = port
		self.address = socket.gethostbyname(address)
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def run(self):
		self.socket.connect((self.address, self.port))
		self.stream = self.socket.makefile('w+')
		self.handle()
		self.finish()

	def finish(self):
		self.socket.shutdown(socket.SHUT_RDWR)
		self.socket.close()

	def handle(self):
		self.stream.write('Hello\n')

class HTTPClient(TCPClient):
	def __init__(self, address, port, path):
		TCPClient.__init__(self, address, port)
		self.host = address
		self.path = path
	def handle(self):
		self.stream.write('GET {} HTTP/1.0\r\n'.format(self.path))
		self.stream.write('Host: {}\r\n'.format(self.host))
		self.stream.write('\r\n')
		self.stream.flush()
		
		data = self.stream.readline()
		while data:
			sys.stdout.write(data)
			data = self.stream.readline()


if __name__ == '__main__':

	for PROCESS in range(PROCESSES):
		pid = os.fork()
		if pid == 0:
			totlag = 0
			for REQUEST in range(REQUESTS):
				client = HTTPClient(URL , PORT, '/test3.img')
				nowtime = time.time()
				client.run()
				endtime= time.time()
				lag=endtime - nowtime
				totlag = totlag+lag
				avglag = totlag/REQUESTS
			#print "\n","Process: ",PROCESS,"average lag",avglag
			print "\n",PROCESS,avglag, "lag"
			sys.exit(0)._exit()
		else:
			signal.signal(signal.SIGCHLD,sigchld_handler)
