set terminal png truecolor
set xrange [0:11]
set yrange [0:4]
set grid
set style data histograms
set style fill solid border
set boxwidth 0.95
set xtics 1,1,10
set output "queue_lag.png"

plot 'queue_lag_data.txt' using ($1):2 title "Process" with boxes

