#!/bin/sh

awk -F " " '

BEGIN{ num = 0; lag_1=0; lag_2 =0;lag_3 =0;lag_4 =0;lag_5 =0;lag_6 =0;lag_7 =0;lag_8 =0;lag_9 =0;lag_10=0; }

{
n++;
if ( n == 1 || n == 11 || n == 21){
	lag_1+= $2;
}
if ( n == 2 || n == 12 || n == 22){
        lag_2+= $2;
}
if ( n == 3 || n == 13 || n == 23){
        lag_3+= $2;
}
if ( n == 4 || n == 14 || n == 24){
        lag_4+= $2;
}
if ( n == 5 || n == 15 || n == 25){
        lag_5+= $2;
}
if ( n == 6 || n == 16 || n == 26){
        lag_6+= $2;
}
if ( n == 7 || n == 17 || n == 27){
        lag_7+= $2;
}
if ( n == 8 || n == 18 || n == 28){
        lag_8+= $2;
}
if ( n == 9 || n == 19 || n == 29){
        lag_9+= $2;
}
if ( n == 10 || n == 20 || n == 30){
        lag_10+= $2;
}
}
END{    print "1",lag_1/3
	print "2",lag_2/3
        print "3",lag_3/3
        print "4",lag_4/3
        print "5",lag_5/3
        print "6",lag_6/3
        print "7",lag_7/3
        print "8",lag_8/3
        print "9",lag_9/3
        print "10",lag_10/3
}' forked_lag.txt

	


