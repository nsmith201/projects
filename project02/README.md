Project 02: Distributed Computing
=================================

Please see the [distributed computing project] write-up.

[distributed computing project]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/homework10.html

1. It calculates the md5 hash of every permutation and compares it to the set of hashes we had stored because the algorithm only goes one way. If its equal then it prints the associated characters with the md5 hash that was being compared I tested it pretty much until it gave me real words consistently instead of junk

2. It creates a bunch of tasks depending on the length of prefixes to check and gives them to the workers. It keeps track of what has been attempted by appending each command to the json journal. It recovers by checking if the command is in the log and if it is it skips it. I tested it first by using the shorter passwords to save time and kept giving its results to your server, once it actually started getting passwords right I knew I got it

3. To make a password more complex length is more important than size of the set of characters you used because the amount of possibilites increases exponentially as you add length as we saw in our project
