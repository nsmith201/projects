#!/usr/bin/env python2.7

import sys
import work_queue
import string
import itertools
import json
import os
# CONSTANTS

LENGTH = 5
HASHES = 'hashes.txt'
SOURCES = ('hulk.py', HASHES)
PORT = 9746
ALPHABET = string.ascii_lowercase + string.digits
derp = open('journal.json','rw')
CHECK = []
JOURNAL = {}
stream = derp.readline()
while stream:
	data = stream.split('"')
	CHECK.append(data[1])
	stream = derp.readline()
if __name__ == '__main__':
	queue = work_queue.WorkQueue(PORT, name='fury-nsmith9', catalog=True)
	queue.specify_log('fury.log')

	for PREFIX in itertools.product(ALPHABET, repeat = 3):
		PREFIX = ''.join(PREFIX)
		print PREFIX
		command = './hulk.py -l {} -s {} -p {}'.format(LENGTH, HASHES, PREFIX)
		if command not in CHECK:
			task = work_queue.Task(command)
		else:
			print "already done"

		for source in SOURCES:
			task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

		queue.submit(task)

	while not queue.empty():
		task = queue.wait()

		if task and task.return_status == 0:
			sys.stdout.write(task.output)	
			sys.stdout.flush()
			JOURNAL[task.command] = task.output.split()
			with open('journal.json.new', 'w') as stream:
				json.dump(JOURNAL, stream)
				os.rename('journal.json.new', 'journal.json')

