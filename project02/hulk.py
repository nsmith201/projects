#!/usr/bin/env python2.7

import itertools
import hashlib
import string
import sys
import random
import getopt
#CONSTANTS

ALPHABET = string.ascii_lowercase + string.digits
LENGTH = 1
PREFIX = ''
HASHES = 'hashes.txt'
PRE = False
try:
        opts, args = getopt.getopt(sys.argv[1:], 'ha:l:s:p:')

except getopt.GetoptError as e:
        print e
        usage(1)

for o, a in opts:
        if o == '-a':
                ALPHABET = a
        if o == '-l':
                LENGTH = int(a)
        if o == '-s':
                HASHES = a
        if o == '-p':
                PREFIX = a
		PLENGTH = len(a)
		PRE = True
        elif o in '-h':
                print "usage ./spidey.py -p [PORT] -d (default='.') [DOCROOT]"
                print " -h show help -f forking mode -v logging to debug"
                sys.exit(1)


def md5sum(s):

	return hashlib.md5(s).hexdigest()

#main

if __name__ == '__main__':
	if not PRE:
		hashes = set([l.strip() for l in open(HASHES)])

		for candidate in itertools.product(ALPHABET, repeat = LENGTH):
			candidate = ''.join(candidate)
			checksum = md5sum(candidate)
	
			if checksum in hashes:
				print candidate	
		sys.exit(0)
	else:
		hashes = set([l.strip() for l in open(HASHES)])

		for candidate in itertools.product(ALPHABET, repeat = LENGTH):
			candidate = PREFIX + ''.join(candidate)
			checksum = md5sum(candidate)

			if checksum in hashes:
				if candidate.startswith( PREFIX ):
					print candidate
					
		sys.exit(0)
